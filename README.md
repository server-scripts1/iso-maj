# Make archlinux's iso updates as simple as a package updating
## Overview
More than a script that allow you to build your custom *__archlinux.iso__* every week, __iso-maj__ also pack your iso in a package *__.pkg.tar.xz__* and put it on a cache-server directory to make it available by your client's simple pacman update.  

## Configuration
There is a configuration file in the repo called __iso-maj.conf__  
There is a few variables to configure before use : 
* __BUILD_DIR_PATH__ witch is the location of the iso and the package build directory.
* __PKG_DIR_PATH__ witch is the location given in the *PKGBUILD* where the iso will be extract from the pkg after installation on your client's computer.
*  __PKG_DIR_OWNER__ witch is the owner (but also the group in the script) in the *PKGBUILD* of the installed iso on your client's computer. 
* __CACHE_DIR_PATH__ witch is the final location where the package is copied (here the cache server)
* __POST_SCRIPT__ the location of a post installation script (optional)

## Requirement
* archiso pkg is installed
* sudo privilege for the user running the script
* the script location must not be in a sub directory of the root directory (doesn't work properly elsewise) 

## Automatize
The repo also contain a systemd service file __iso-maj.service__ witch can be use for scheduling the launch of the script.
